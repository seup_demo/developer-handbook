![Build Status](https://gitlab.com/seup/developer-handbook/badges/master/pipeline.svg)

---

Contains the source files for SEUP Developer Handbook. 

---

## Current Version

0.1.0


## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```
image: alpine

pages:
  script:
  - apk --no-cache add py2-pip python-dev
  - pip install sphinx
  - apk --no-cache add make
  - make html
  - mv _build/html/ public/
  artifacts:
    paths:
    - public
  only:
  - master
```

## Requirements

- [Sphinx][]

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install][sphinx] Sphinx
1. Generate the documentation: `make`

The generated HTML will be located in the location specified by `conf.py`,
in this case `_build/html`.

## Did you fork this project?

If you forked this project for your own use, please go to your project's
**Settings** and remove the forking relationship, which won't be necessary
unless you want to contribute back to the upstream project.

---

Forked from https://gitlab.com/Eothred/sphinx

[ci]: https://about.gitlab.com/gitlab-ci/
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages
[sphinx]: http://www.sphinx-doc.org/
