Project Management Basics
=========================

Communication
-------------
* For team communication, we use Slack. This is best used for a discussion, quick question etc. If you haven't received an invite already, please send an email to the admin. 

.. _onlineRepo:

Version Control and Online Repository
-------------------------------------
* For version control we use Git and GitLab. GitLab also serves as our online repository (repo). 
* Each project has its own repo. If you do not have a repo for your project, send an email to the admin. 
* Repos are not just restricted to hardware and software projects. They can be used for design documents, technical reports etc. 
* Refrain from creating new project repos unless you've been directed by the project lead.
* If you haven't used Git or GitLab or version control in general, a tutorial will be provided.

Issue Management
----------------
* GitLab will be used to raise and manage issues. Please raise issues in the appropriate repo.
* Issues can be used for bugs, product planning, documentation, new features etc. 
* Leave the issue assignee 'unassigned' if you do not know how is responsible for solving it. 
* To learn more about GitLab issues, see `here <https://docs.gitlab.com/ee/user/project/issues/>`_ .

Drive Repo for planning and data dump
-------------------------------------
* Every project usually has a repository on Google Drive for planning.
* This can be used for preliminary planning and data dump. 
* Once a base version of a document is available, it should be moved to GitLab and converted to `LateX <https://www.latex-project.org/>`_ or `Sphinx <http://www.sphinx-doc.org/>`_  
* Contact the admin/project lead for Drive repo details. 
* **NOTE**: Google Drive should only be used as a staging area and not used for 'production' hardware, software, documentation etc. 

.. _scheduling:

Scheduling
----------
* Before you start your project, please divide the project into number of tasks/modules. Estimate the number of hours/days it would take you to complete these tasks/modules. Double this number to get a more realistic number.
* Download `this <https://drive.google.com/open?id=1kWkvWwd7oVIanAdwNenHM4-0m0hLn36g-kHKo-dirpo>`_ template and use it for scheduling your project development. Make it as detailed as possible. 


Documentation Guidelines
------------------------
Project documentation is as important as the code, design etc. and must be done **hand-in-hand with the product development**. 

Before-you-start-development checklist
--------------------------------------
* You have access to the repository (:ref:`onlineRepo`). 
* You have made a list of project tasks and timeline (:ref:`scheduling`)
* You have written a base version of SRS (:ref:`srs`) / Functional Spec (:ref:`functionalSpec`) and/or Tech Specs (:ref:`techSpec`) in `Sphinx <http://www.sphinx-doc.org/>`_ or `LateX <https://www.latex-project.org/>`_.
* Make sure you've setup your documentation tool (:ref:`doc`).