Welcome to SEUP Developer Handbook (SDH)
========================================

Handbook for developers, designers, engineers, writers etc. working in the `Simple Electric Utility Platform (SEUP) <https://seupdocs.readthedocs.io>`_ project. Contains info on documentation, repository organization, project management, project and design guidelines etc. 

A PDF version of this manual can be found `here <https://gitlab.com/seup/developer-handbook/blob/master/DeveloperHandbook.pdf>`_.

*Why SDH?*

* More professional approach to product development and maintenance
* Follow good industry practices 
* Better project management
* Good documentation for users, current and future developers
* Aligning project for potential community (or large team) collaboration 
* Increase product reliability and performance through better pre (definition and requirement specs) and post (testing) product development practices.
* Allow addition of `continuous integration (CI) and continuous deployment (CD) <https://www.digitalocean.com/community/tutorials/an-introduction-to-continuous-integration-delivery-and-deployment>`_ of SEUP software for better testing and generating builds.

-----

**Contents**:

.. toctree::
   :maxdepth: 2

   projectManage
   repo
   productDev
   documentation
   other
   codeReview
   testing





.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`search`


Bugs? Questions?
================
Raise an issue `here <https://gitlab.com/seup/developer-handbook>`_.


Admin and Project Maintainer
============================
Ashray Manur
ashraymanur@gmail.com